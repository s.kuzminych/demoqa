# DemoQA
### DemoQA: 
- Contains Drag-and-Drop action testing using xUnit and POM (Page Object Model). 

##### Steps to reproduce: #####
1. Open [DemoQA](https://demoqa.com/).
2. Click on "Droppable" on the side menu.
3. Move draggable object to droppable object.
4. Check that the color of droppable object has turned yellow.
5. Check that droppable object contains "Dropped!".

- [Test Method](https://gitlab.com/s.kuzminych/demoqa/blob/master/DemoQA/Tests/DroppablePositiveTest.cs).
***
### Locators Practice:
- Contains locators for depicted elements on [Github](https://github.com/). You must not be logged in. 

##### Locators: #####
1. ![First element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/1.png)
- `//a[contains(@href, 'github.com/github')]/parent::li`
2. ![Second element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/2.png)
- `//header//form[@role="search"]/label/img`
- `//header//input[@name='q']/following-sibling::img`
- `//header//input[@name='type']/following-sibling::img`
3. ![Third element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/3.png)
- `//form[not(@aria-label='Sign up')]//a[contains(@href, '/terms')]`
4. ![Fourth element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/4.png)
- `//a[@href="/topics"]`
5. ![Fifth element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/5.png)
- `//h2/following-sibling::div/a`
6. ![Sixth element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/6.png)
- `//a[@href='/customer-stories/sap']//span[not (@*)]`
7. ![Seventh element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/7.png)
- `//div[contains(@class, "apps-cluster")]/div[@aria-label="Google"]`
- `//div[contains(@class, "apps-cluster")]//img[@alt="Google"]/..`
8. ![Eighth element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/8.png)
- `//a[@href='/personal']//p`
9. ![Ninth element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/9.png)
- `//form[@aria-label='Sign up']//button`
10. ![Tenth element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/10.png)
- `//footer//a[contains(@data-ga-click, "go to home")]`
- `//footer//a[@href="/"]`
11. ![Eleventh element](https://gitlab.com/s.kuzminych/demoqa/tree/master/ElementsToFindOnGithub/11.png)
- `//*[@id="jump-to-results"]`