using DemoQA.Browsers;
using DemoQA.PageObjects;
using System;
using Xunit;

namespace DemoQA
{
    public class Tests : IDisposable
    {
        private readonly string ExpectedDroppableObjectBackgroundColor = "rgb(255, 250, 144)";
        private readonly string ExpectedDroppableObjectText = "Dropped!";
        private Browser Browser;
        public Tests()
        {
            Browser = Browser.GetInstance();
        }
        [Fact]
        public void DroppablePositiveTest()
        {
            Browser.OpenURL("https://demoqa.com/");
            HomePage homePage = new HomePage(Browser);
            DroppablePage droppablePage = (DroppablePage)homePage.InteractionLinkClick("Droppable");
            droppablePage.DropObject();
            Assert.StartsWith(ExpectedDroppableObjectBackgroundColor, droppablePage.DroppableObjectBackgroundColor);
            Assert.Equal(ExpectedDroppableObjectText, droppablePage.DroppableObjectText);
        }
        public void Dispose()
        {
            Browser.Quit();
        }
    }
}
