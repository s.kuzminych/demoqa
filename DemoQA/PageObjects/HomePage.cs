﻿using DemoQA.Browsers;
using OpenQA.Selenium;

namespace DemoQA.PageObjects
{
    public class HomePage : BasePage
    {
        public HomePage(Browser browser) : base (browser) { }

        #region Elements

        private IWebElement GetInteractionsLink(string linkText) => Browser.FindElement(By.XPath($"//div[@id='sidebar']//aside//*[text()='Interactions']/following-sibling::ul//a[text()='{linkText}']"));

        #endregion

        #region Methods

        public BasePage InteractionLinkClick(string linkName)
        {
            Browser.Click(GetInteractionsLink("Droppable"));
            return new DroppablePage(Browser);
        }

        #endregion
    }
}
