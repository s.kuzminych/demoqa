﻿using DemoQA.Browsers;
using OpenQA.Selenium;

namespace DemoQA.PageObjects
{
    public class DroppablePage : BasePage
    {
        public DroppablePage(Browser browser) : base(browser) { }

        #region Elements

        private IWebElement DraggableObject => Browser.FindElement(By.Id("draggable"));
        private IWebElement DroppableObject => Browser.FindElement(By.Id("droppable"));
        public string DroppableObjectBackgroundColor => DroppableObject.GetCssValue("background");
        public string DroppableObjectText => DroppableObject.Text;

        #endregion

        #region Methods

        public void DropObject()
        {
            Browser.DragAndDrop(DraggableObject, DroppableObject);
        }

        #endregion
    }
}
