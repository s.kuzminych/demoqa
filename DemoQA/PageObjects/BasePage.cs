﻿using DemoQA.Browsers;

namespace DemoQA.PageObjects
{
    public abstract class BasePage
    {
        protected Browser Browser { get; }
        protected BasePage(Browser browser)
        {
            Browser = browser;
        }
    }
}
