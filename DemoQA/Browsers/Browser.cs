﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DemoQA.Browsers
{
    public class Browser
    {
        private IWebDriver Driver { get; }
        private Browser()
        {
            Driver = new ChromeDriver($"{Environment.CurrentDirectory}\\Resources");
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        private static Browser Instance;
        public static Browser GetInstance()
        {
            if (Instance == null)
            {
                Instance = new Browser();
            }
            return Instance;
        }

        #region Methods

        public IWebElement FindElement(By by)
        {
            List<IWebElement> element = Instance.Driver.FindElements(by).ToList();
            if (element.Count != 1)
            {
                throw new NoSuchElementException("It is not only one element");
            }
            return element[0];
        }
        public List<IWebElement> FindElements(By by)
        {
            List<IWebElement> elements = Instance.Driver.FindElements(by).ToList();
            if (elements.Count == 0)
            {
                throw new NoSuchElementException("There is no such elements");
            }
            return elements;
        }
        public void Click(IWebElement element) => element.Click();
        public void DragAndDrop(IWebElement source, IWebElement target) => new Actions(Driver).DragAndDrop(source, target).Build().Perform();
        public void OpenURL(string URL) => Driver.Navigate().GoToUrl(URL);
        public void Quit()
        {
            Driver.Quit();
            Instance = null;
        }

        #endregion

    }
}
